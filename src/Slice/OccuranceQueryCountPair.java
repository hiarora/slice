package Slice;

/**
 * OccuranceQueryCountPair class. This class is instantiated for every word that occurs in the file list. 
 */

public class OccuranceQueryCountPair {

	private int occurances;
	private int queries;
	
	public OccuranceQueryCountPair(){
		this.setOccurances(0);
		this.setQueries(0);
	}
	
	/**
     *  @usage	   increases occurrence count by 1
   	 */
	public void incrementOccurances(){
		this.setOccurances(this.getOccurances() + 1);
	}
	/**
     *  @usage	   increases query count by 1
   	 */	
	public void incrementQueries(){
		this.setQueries(this.getQueries() + 1);
	}
	/**
     *  @return	   returns number of queries for the associated word 
   	 */	
	public int getQueries() {
		return queries;
	}
	/**
     *  @param	   sets number of queries
   	 */	
	public void setQueries(int queries) {
		this.queries = queries;
	}
	/**
     *  @return	   returns number of occurrence for the associated word 
   	 */	
	public int getOccurances() {
		return occurances;
	}
	/**
     *  @param	   sets number of occurances
   	 */
	public void setOccurances(int occurances) {
		this.occurances = occurances;
	}
	/**
     *  return 	   stringified version of object in the form form { <numqueries>, <occurance> }
   	 */
	 @Override
	 public String toString() {
		String returnString = "{ ";
		returnString += Integer.toString(this.getQueries());
		returnString += " , ";
		returnString += Integer.toString(this.getOccurances()) + " }";
		return returnString;
	 }

}
