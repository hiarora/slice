package Slice;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class SliceTest
 */@WebServlet("/SliceTest")
public class SliceTest extends HttpServlet {

	// stores each word and it's occurrence and number of times it has been queried.
	private HashMap < String, OccuranceQueryCountPair > myWordMap = new HashMap < String, OccuranceQueryCountPair > ();

	private static final long serialVersionUID = 1L;

	// for singleton initialization
	private static boolean isInitialised = false;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public SliceTest() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter pw = response.getWriter();
		if (request.getParameterMap().containsKey("word") != false) {
			initialiseOccuranceQueryMap();
			String myWord = request.getParameter("word").toString();
			OccuranceQueryCountPair myOccuranceQueryPair = getWordCount(myWord.toLowerCase());
			pw.print(myOccuranceQueryPair.toString());
		}
	}

	/**
	 *  @usage	   to construct the hash map containing of the type map<word, <occurance, numqueries>>
	 */
	protected void initialiseOccuranceQueryMap() throws IOException {
		if (!isInitialised) {
			myWordMap.clear();
			isInitialised = true;
			ArrayList < String > myFileList = getFileList();
			int c = 0;
			for (String file: myFileList) {
				System.out.println(file + Integer.toString(++c));
				tokeniseFileAndUpdateMap(file);
			}
		}
	}

	/**
	 *  @usage		returns number of times a word is queried and it's count  
	 *  @param      queryWord return count of this word
	 *  @return     OccuranceQueryCountPair. Returns new pair of word queried for the first time else 
	 *  			increments the queries of the word that has been queried and returns the count, query pair
	 */
	protected OccuranceQueryCountPair getWordCount(String queryWord) {
		OccuranceQueryCountPair pair;
		if (myWordMap.containsKey(queryWord)) {
			pair = myWordMap.get(queryWord);
			pair.incrementQueries();
		} else {
			OccuranceQueryCountPair myPair = new OccuranceQueryCountPair();
			myWordMap.put(queryWord, myPair);
			myPair.incrementQueries();
			pair = myPair;
		}
		return pair;
	}

	/**
	 *  @usage      returns an ArrayList of files in the /WEB-INF/ directory
	 *  @return     list of files in the /WEB-INF/ directory
	 */
	protected ArrayList < String > getFileList() throws IOException {
		String realPath = getServletContext().getRealPath("/WEB-INF/");
		File folder = new File(realPath + (isWindows() ? "\\txtfiles" : "/txtfiles"));
		File[] listOfFiles = folder.listFiles();
		ArrayList < String > fileList = new ArrayList < > ();
		for (File file: listOfFiles) {
			if (file.isFile()) {
				fileList.add(file.getPath());
			}
		}
		return fileList;
	}

	/**
	 *  @param      path The path of file whose words need to be hashed
	 *  @usage      call this function to add the words of the file pass in the hashmap
	 */
	protected void tokeniseFileAndUpdateMap(String path) throws IOException {
		FileReader fr = new FileReader(path);
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		while ((line = br.readLine()) != null) {
			String[] words = line.split("\\W+"); //splits if not an alphanumeric character.
			for (int i = 0; i < words.length; i++) {
				words[i] = words[i].replaceAll("[^\\w]", "");
				insertIntoMap(words[i].toLowerCase());
			}
		}
		br.close();
	}

	/**
	 *  @param      myWord the word that needs to hashed in the map. 
	 *  @usage      call this function to add a word to map. Will create a new 
	 *  			instance and increment the occurance by 1 if not found, else
	 *  			return the original instance 
	 */
	protected void insertIntoMap(String myWord) {
		if (myWordMap.containsKey(myWord)) {
			myWordMap.get(myWord).incrementOccurances();
		} else {
			OccuranceQueryCountPair myPair = new OccuranceQueryCountPair();
			myPair.incrementOccurances();
			myWordMap.put(myWord, myPair);
		}
	}

	/**
	 *  @return     returns the os type
	 */
	public static String getOsName() {
		return System.getProperty("os.name");
	}

	/**
	 *  @return      return true if windows else false.
	 */
	public static boolean isWindows() {
		return getOsName().startsWith("Windows");
	}


}